<?php

require '../vendor/autoload.php';

$app= new \Slim\App();

$app->get('/', function (\Slim\Http\Request $request,\Slim\Http\Response $response){
	$body = file_get_contents('employees.json');
    $contacto = json_decode($body);

    $response->getBody()->write("<center><h2><strong>List employees</strong></h2></center><br>");
    $response->getBody()->write("<center><form action='index.php/search' method='post'> <input type='text' name='smail' placeholder='Email'><input type='submit' value='Search'></form></center>");
    $response->getBody()->write("<center><table><thead><tr>
		<td><strong>NAME</strong></td>
		<td><strong>EMIAL</strong></td>
		<td><strong>POSITION</strong></td>
		<td><strong>SALARY</strong></td>
		<td><strong>DETAIL</strong></td>
    	</tr></thead><tbody>");
    foreach ($contacto as $key => $value) {

    	$response->getBody()->write("<tr>");
    	$response->getBody()->write('<td>'.$value->name."</td>");
    	$response->getBody()->write('<td>'.$value->email."</td>");
    	$response->getBody()->write('<td>'.$value->position."</td>");
    	$response->getBody()->write('<td>'.$value->salary."</td>");
    	$response->getBody()->write('<td><a href="index.php/detail/'.$key.'">Detail</a></td>');

    	$response->getBody()->write("</tr>");
    	# code...
    }
    $response->getBody()->write("</tbody></table></center>");

	return $response;
});

$app->get('/detail/{id}', function (\Slim\Http\Request $request,\Slim\Http\Response $response){

	$body = file_get_contents('employees.json');
    $contacto = json_decode($body);

    $id = $request->getAttribute('id');
    $response->getBody()->write("<h3><a href='../../index.php'> Back.. </a></h3>");

    
    $response->getBody()->write("<center><h2><strong>Employees detail</strong></h2></center><br>");
    $response->getBody()->write("<b>Name: </b>".$contacto[$id]->name."<br>");
    $response->getBody()->write("<b>Email: </b>".$contacto[$id]->email."<br>");
    $response->getBody()->write("<b>Phone: </b>".$contacto[$id]->phone."<br>");
    $response->getBody()->write("<b>Address: </b>".$contacto[$id]->address."<br>");
    $response->getBody()->write("<b>Position: </b>".$contacto[$id]->position."<br>");
    $response->getBody()->write("<b>Salary: </b>".$contacto[$id]->salary."<br>");
    $response->getBody()->write("<b>Skills: </b><br>");	

    foreach ($contacto[$id]->skills as $key => $value) {
    	$response->getBody()->write(" - ".$value->skill."<br>");	
    }

	return $response;
});


$app->post('/search', function (\Slim\Http\Request $request,\Slim\Http\Response $response){

	$body = file_get_contents('employees.json');
    $contacto = json_decode($body);

    $lst = array();
    foreach ($contacto as $key => $value) {
    		$lst[$key]=$value->email;
    }
    $semail = $request->getParsedBody()['smail'];
    $rs=array_search($semail, $lst,true);

        $response->getBody()->write("<h3><a href='../'> Back.. </a></h3>");
        if ($rs==0) {
            $rs='0a';
        }
    if ($rs) {
        $rs=explode('a', $rs);
    	$response->getBody()->write("<center><h2><strong>Result</strong></h2></center><br>");
	    $response->getBody()->write("<b>Name: </b>".$contacto[$rs[0]]->name."<br>");
	    $response->getBody()->write("<b>Email: </b>".$contacto[$rs[0]]->email."<br>");
	    $response->getBody()->write("<b>Phone: </b>".$contacto[$rs[0]]->phone."<br>");
	    $response->getBody()->write("<b>Address: </b>".$contacto[$rs[0]]->address."<br>");
	    $response->getBody()->write("<b>Position: </b>".$contacto[$rs[0]]->position."<br>");
	    $response->getBody()->write("<b>Salary: </b>".$contacto[$rs[0]]->salary."<br>");
	    $response->getBody()->write("<b>Skills: </b><br>");	

	    foreach ($contacto[$rs[0]]->skills as $key => $value) {
	    	$response->getBody()->write(" - ".$value->skill."<br>");	
	    }
    }else{
    	    $response->getBody()->write("<center><h2><strong>No result...</strong></h2></center><br>");
    }
	
	return $response;
});

$app->get('/xml/{minimo}/{maximo}', function (\Slim\Http\Request $request,\Slim\Http\Response $response){


	$body = file_get_contents('employees.json');
    $contacto = json_decode($body);

    $minimo = $request->getAttribute('minimo');
    $maximo = $request->getAttribute('maximo');


		  $response->withHeader('Content-type', 'text/xml');
		  $xml = new SimpleXMLElement('<response/>');
		 
		  foreach ($contacto as $key => $value) {
		  	$s1 = str_replace("$", "", $value->salary);
		  	$sf = str_replace(",", "", $s1);
		  	if ($sf>=$minimo && $sf<=$maximo) {
		  		$item = $xml->addChild('item');
		    	$item->addChild('name', $value->name);
		    	$item->addChild('salary', $value->salary);
		  	}
		  }
		  $new=$xml->asXml();

		  return $new;

});

$app->run();